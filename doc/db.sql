-- DB Scripts
-- Modified wrong foreign key pointing to Status when it was supposed to point to Type

--USE [industrialization]
--GO
/****** Object:  Table [dbo].[screen]    Script Date: 23/11/2017 20:11:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[screen](
	[screenId] [int] IDENTITY(1,1) NOT NULL,
	[screenName] [varchar](150) NOT NULL,
	[screenDescription] [text] NOT NULL,
	[screenPath] [varchar](250) NOT NULL,
	[screenIsAdmin] [bit] NOT NULL,
	[screenThumbPath] [varchar](250) NOT NULL,
	[screenActive] [bit] NULL,
 CONSTRAINT [PK_screen] PRIMARY KEY CLUSTERED
(
	[screenId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user]    Script Date: 23/11/2017 20:11:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user](
	[userID] [int] IDENTITY(1,1) NOT NULL,
	[userTypeId] [int] NOT NULL,
	[userName] [varchar](150) NOT NULL,
	[userEmail] [varchar](250) NOT NULL,
	[userNetworkLogin] [varchar](100) NULL,
	[userPassword] [varchar](250) NULL,
	[userPhone] [varchar](50) NOT NULL,
	[userStatusId] [int] NOT NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[userStatus]    Script Date: 23/11/2017 20:11:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[userStatus](
	[userStatusId] [int] IDENTITY(1,1) NOT NULL,
	[userStatusName] [varchar](100) NOT NULL,
	[userStatusCode] [varchar](50) NOT NULL,
	[userStatusActive] [bit] NULL CONSTRAINT [DF_userStatus_userStatusActive]  DEFAULT ((1)),
 CONSTRAINT [PK_userStatus] PRIMARY KEY CLUSTERED
(
	[userStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[userType]    Script Date: 23/11/2017 20:11:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[userType](
	[userTypeId] [int] IDENTITY(1,1) NOT NULL,
	[userTypeName] [varchar](150) NOT NULL,
	[userTypeCode] [varchar](50) NOT NULL,
	[userTypeActive] [bit] NULL CONSTRAINT [DF_userType_userTypeActive]  DEFAULT ((1)),
 CONSTRAINT [PK_userType] PRIMARY KEY CLUSTERED
(
	[userTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

-- Modified from original script
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[user]  WITH CHECK ADD  CONSTRAINT [FK_user_userStatus] FOREIGN KEY([userStatusId])
REFERENCES [dbo].[userStatus] ([userStatusId])
GO
ALTER TABLE [dbo].[user] CHECK CONSTRAINT [FK_user_userStatus]
GO
ALTER TABLE [dbo].[user]  WITH CHECK ADD  CONSTRAINT [FK_user_userType] FOREIGN KEY([userTypeId])
REFERENCES [dbo].[userType] ([userTypeId])
GO
ALTER TABLE [dbo].[user] CHECK CONSTRAINT [FK_user_userType]
GO

-- enjoy
