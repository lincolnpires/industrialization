using Industrialization.Domain.Entities;
using Industrialization.Infrastructure.DataAccess.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Industrialization.Infrastructure.DataAccess.Concrete
{
    public class UserRepository : AbstractRepository<User>, IUserRepository
    {
        public UserRepository(AppDbContext dbContext) : base(dbContext) { }

        public User GetUserByEmail(string email)
            => base.dbContext.Set<User>().SingleOrDefault(x => x.Email == email);
    }
}
