using Industrialization.Domain.Entities;
using Industrialization.Infrastructure.DataAccess.Abstract;

namespace Industrialization.Infrastructure.DataAccess.Concrete
{
    public class TypeRepository : AbstractRepository<UserType>, ITypeRepository
    {
        public TypeRepository(AppDbContext dbContext) : base(dbContext) { }
    }
}
