using Industrialization.Domain.Entities;
using Industrialization.Infrastructure.DataAccess.Abstract;

namespace Industrialization.Infrastructure.DataAccess.Concrete
{
    public class ScreenRepository : AbstractRepository<Screen>, IScreenRepository
    {
        public ScreenRepository(AppDbContext dbContext) : base(dbContext) { }
    }
}
