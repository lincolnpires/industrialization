using Industrialization.Domain.Entities;
using Industrialization.Infrastructure.DataAccess.Mappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Linq;

namespace Industrialization.Infrastructure.DataAccess.Concrete
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public virtual DbSet<Screen> Screen { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<UserType> Types { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // hum looks like there's no way to add mappings from assembly
            // https://docs.microsoft.com/en-us/dotnet/api/microsoft.entityframeworkcore.modelbuilder?view=efcore-2.0

            // ¯\_(ツ)_/¯ So be it
            modelBuilder.ApplyConfiguration(new ScreenMap());
            modelBuilder.ApplyConfiguration(new UserMap());
            modelBuilder.ApplyConfiguration(new TypeMap());
            modelBuilder.ApplyConfiguration(new StatusMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}
