using Industrialization.Domain.Entities;
using Industrialization.Infrastructure.DataAccess.Abstract;

namespace Industrialization.Infrastructure.DataAccess.Concrete
{
    public class StatusRepository : AbstractRepository<Status>, IStatusRepository
    {
        public StatusRepository(AppDbContext dbContext) : base(dbContext) { }
    }
}
