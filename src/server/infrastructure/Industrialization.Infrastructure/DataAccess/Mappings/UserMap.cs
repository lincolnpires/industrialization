using Industrialization.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Industrialization.Infrastructure.DataAccess.Mappings
{
    internal class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("user");

            builder.Property(e => e.Id).HasColumnName("userID");

            builder.Property(e => e.Email)
                .IsRequired()
                .HasColumnName("userEmail")
                .HasMaxLength(250)
                .IsUnicode(false);

            builder.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("userName")
                .HasMaxLength(150)
                .IsUnicode(false);

            builder.Property(e => e.NetworkLogin)
                .HasColumnName("userNetworkLogin")
                .HasMaxLength(100)
                .IsUnicode(false);

            builder.Property(e => e.Password)
                .HasColumnName("userPassword")
                .HasMaxLength(250)
                .IsUnicode(false);

            builder.Property(e => e.Phone)
                .IsRequired()
                .HasColumnName("userPhone")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.StatusId).HasColumnName("userStatusId");

            builder.Property(e => e.TypeId).HasColumnName("userTypeId");

            builder.HasOne(d => d.Status)
                .WithMany(p => p.Users)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_user_userStatus");

            builder.HasOne(d => d.UserType)
                .WithMany(p => p.Users)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_user_userType");
        }
    }
}
