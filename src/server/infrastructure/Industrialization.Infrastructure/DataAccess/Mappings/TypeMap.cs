using Industrialization.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Industrialization.Infrastructure.DataAccess.Mappings
{
    internal class TypeMap : IEntityTypeConfiguration<UserType>
    {
        public void Configure(EntityTypeBuilder<UserType> builder)
        {
            builder.ToTable("userType");

            builder.Property(e => e.Id).HasColumnName("userTypeId");

            builder.Property(e => e.Active)
                .HasColumnName("userTypeActive")
                .HasDefaultValueSql("((1))");

            builder.Property(e => e.Code)
                .IsRequired()
                .HasColumnName("userTypeCode")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("userTypeName")
                .HasMaxLength(150)
                .IsUnicode(false);
        }
    }
}
