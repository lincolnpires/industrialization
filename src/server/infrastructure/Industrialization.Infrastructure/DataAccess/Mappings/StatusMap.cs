using Industrialization.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Industrialization.Infrastructure.DataAccess.Mappings
{
    public class StatusMap : IEntityTypeConfiguration<Status>
    {
        public void Configure(EntityTypeBuilder<Status> builder)
        {
            builder.ToTable("userStatus");

            builder.Property(e => e.Id).HasColumnName("userStatusId");

            builder.Property(e => e.Active)
                .HasColumnName("userStatusActive")
                .HasDefaultValueSql("((1))");

            builder.Property(e => e.Code)
                .IsRequired()
                .HasColumnName("userStatusCode")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("userStatusName")
                .HasMaxLength(100)
                .IsUnicode(false);
        }
    }
}
