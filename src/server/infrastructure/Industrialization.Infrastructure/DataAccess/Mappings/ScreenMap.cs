using Industrialization.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Industrialization.Infrastructure.DataAccess.Mappings
{
    internal class ScreenMap : IEntityTypeConfiguration<Screen>
    {
        public void Configure(EntityTypeBuilder<Screen> builder)
        {
            builder.ToTable("screen");

                builder.Property(e => e.Id).HasColumnName("screenId");

                builder.Property(e => e.Active).HasColumnName("screenActive");

                builder.Property(e => e.IsAdmin).HasColumnName("screenIsAdmin");

                builder.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("screenDescription")
                    .HasColumnType("text");

                builder.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("screenName")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                builder.Property(e => e.Path)
                    .IsRequired()
                    .HasColumnName("screenPath")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                builder.Property(e => e.ThumbPath)
                    .IsRequired()
                    .HasColumnName("screenThumbPath")
                    .HasMaxLength(250)
                    .IsUnicode(false);
        }
    }
}
