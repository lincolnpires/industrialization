using Industrialization.Domain.Entities;

namespace Industrialization.Infrastructure.DataAccess.Abstract
{
    public interface ITypeRepository : IRepository<UserType> { }
}
