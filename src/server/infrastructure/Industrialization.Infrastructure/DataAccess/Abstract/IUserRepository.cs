using Industrialization.Domain.Entities;

namespace Industrialization.Infrastructure.DataAccess.Abstract
{
    public interface IUserRepository : IRepository<User>
    {
        User GetUserByEmail(string email);
    }
}
