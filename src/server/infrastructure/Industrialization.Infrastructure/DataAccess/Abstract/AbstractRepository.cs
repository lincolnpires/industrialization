using Industrialization.Domain.Abstract;
using Industrialization.Infrastructure.DataAccess.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace Industrialization.Infrastructure.DataAccess.Abstract
{
    public class AbstractRepository<T> : IDisposable, IRepository<T> where T : Entity
    {
        protected readonly AppDbContext dbContext;

        public AbstractRepository(AppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public T GetOneById(int id) => dbContext.Set<T>().SingleOrDefault(x => x.Id == id);

        public IQueryable<T> GetMany(Expression<Func<T, bool>> predicate)
            => dbContext.Set<T>().AsNoTracking().Where(predicate).AsQueryable();

        public IQueryable<T> GetAll() => dbContext.Set<T>().AsNoTracking().AsQueryable();

        public void Add(T entity)
        {
            dbContext.Set<T>().Add(entity);
            dbContext.SaveChanges();
        }

        public void Update(T entity)
        {
            dbContext.Entry(entity).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        public void Remove(T entity)
        {
            dbContext.Set<T>().Remove(entity);
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            dbContext.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
