using Industrialization.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Industrialization.Infrastructure.DataAccess.Abstract
{
    public interface IRepository<T> where T : Entity
    {
        T GetOneById(int id);
        IQueryable<T> GetMany(Expression<Func<T, bool>> predicate);
        IQueryable<T> GetAll();
        void Add(T entity);
        void Update(T entity);
        void Remove(T entity);
    }
}
