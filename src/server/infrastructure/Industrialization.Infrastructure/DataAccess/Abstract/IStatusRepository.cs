using Industrialization.Domain.Entities;

namespace Industrialization.Infrastructure.DataAccess.Abstract
{
    public interface IStatusRepository : IRepository<Status> { }
}
