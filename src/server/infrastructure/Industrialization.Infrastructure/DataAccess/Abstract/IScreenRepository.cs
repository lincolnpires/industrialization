using Industrialization.Domain.Entities;

namespace Industrialization.Infrastructure.DataAccess.Abstract
{
    public interface IScreenRepository : IRepository<Screen> { }
}
