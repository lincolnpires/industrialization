using Industrialization.Infrastructure.DataAccess.Abstract;
using Industrialization.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;

namespace Industrialization.Services.Controllers
{
    [Route("api/[controller]")]
    public class TypeController : Controller
    {
        private readonly ITypeRepository typeRepository;

        public TypeController(ITypeRepository typeRepository) =>
            this.typeRepository = typeRepository;

        [HttpGet]
        public IActionResult GetAll()
        {
            var items = typeRepository.GetAll();
            return Ok(items);
        }

        [HttpGet("{id}", Name = "GetOneType")]
        public IActionResult GetOne(int id)
        {
            var item = typeRepository.GetOneById(id);

            if (null == item)
            {
                return NotFound();
            }

            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] UserType item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            typeRepository.Add(item);

            // http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
            return CreatedAtRoute("GetOneType", new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] UserType item)
        {
            if (item == null || item.Id != id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var type = typeRepository.GetOneById(id);
            if (type == null)
            {
                return NotFound();
            }

            // TODO: FAT CONTROLLER! Extract method to another class
            if (item.Active.HasValue && item.Active.Value)
            {
                type.Activate();
            }
            else
            {
                type.Deactivate();
            }

            type.Code = item.Code;
            type.Name = item.Name;
            typeRepository.Update(type);

            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var type = typeRepository.GetOneById(id);
            if (type == null)
            {
                return NotFound();
            }

            typeRepository.Remove(type);
            return new NoContentResult();
        }
    }
}
