using Industrialization.Infrastructure.DataAccess.Abstract;
using Industrialization.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;

namespace Industrialization.Services.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserRepository userRepository;

        public UserController(IUserRepository userRepository) =>
            this.userRepository = userRepository;

        [HttpGet]
        public IActionResult GetAll()
        {
            var items = userRepository.GetAll();
            return Ok(items);
        }

        [HttpGet("{id}", Name = "GetOneUser")]
        public IActionResult GetOne(int id)
        {
            var item = userRepository.GetOneById(id);

            if (null == item)
            {
                return NotFound();
            }

            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] User item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            userRepository.Add(item);

            // http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
            return CreatedAtRoute("GetOneUser", new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] User item)
        {
            if (item == null || item.Id != id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var user = userRepository.GetOneById(id);
            if (user == null)
            {
                return NotFound();
            }

            // TODO: ugly fat controller: clean up this mess!
            if (!string.IsNullOrEmpty(item.Password))
            {
                user.SetNetworkLoginAndPassword(item.NetworkLogin, item.Password);
            }

            user.Email = item.Email;
            user.Name = item.Name;
            user.Phone = item.Phone;
            user.StatusId = item.StatusId;
            user.TypeId = item.TypeId;
            userRepository.Update(user);

            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var user = userRepository.GetOneById(id);
            if (user == null)
            {
                return NotFound();
            }

            userRepository.Remove(user);
            return new NoContentResult();
        }
    }
}
