using Industrialization.Infrastructure.DataAccess.Abstract;
using Industrialization.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;

namespace Industrialization.Services.Controllers
{
    [Route("api/[controller]")]
    public class ScreenController : Controller
    {
        private readonly IScreenRepository screenRepository;

        public ScreenController(IScreenRepository screenRepository) =>
            this.screenRepository = screenRepository;

        [HttpGet]
        public IActionResult GetAll()
        {
            var items = screenRepository.GetAll();
            return Ok(items);
        }

        [HttpGet("{id}", Name = "GetOneScreen")]
        public IActionResult GetOne(int id)
        {
            var item = screenRepository.GetOneById(id);

            if (null == item)
            {
                return NotFound();
            }

            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Screen item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            screenRepository.Add(item);

            // http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
            return CreatedAtRoute("GetOneScreen", new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Screen item)
        {
            if (item == null || item.Id != id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var screen = screenRepository.GetOneById(id);
            if (screen == null)
            {
                return NotFound();
            }

            // TODO: FAT CONTROLLER! Encapsulate this in a command or service
            if (item.Active.HasValue && item.Active.Value)
            {
                screen.Activate();
            } else {
                screen.Deactivate();
            }
            screen.IsAdmin = item.IsAdmin;
            screen.Description = item.Description;
            screen.Name = item.Name;
            screen.Path = item.Path;
            screen.ThumbPath = item.ThumbPath;

            screenRepository.Update(screen);

            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var screen = screenRepository.GetOneById(id);
            if (screen == null)
            {
                return NotFound();
            }

            screenRepository.Remove(screen);
            return new NoContentResult();
        }
    }
}
