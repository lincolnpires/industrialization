using Industrialization.Infrastructure.DataAccess.Abstract;
using Industrialization.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;

namespace Industrialization.Services.Controllers
{
    [Route("api/[controller]")]
    public class StatusController : Controller
    {
        private readonly IStatusRepository statusRepository;

        public StatusController(IStatusRepository statusRepository) =>
            this.statusRepository = statusRepository;

        [HttpGet]
        public IActionResult GetAll()
        {
            var items = statusRepository.GetAll();
            return Ok(items);
        }

        [HttpGet("{id}", Name = "GetOneStatus")]
        public IActionResult GetOne(int id)
        {
            var item = statusRepository.GetOneById(id);

            if (null == item)
            {
                return NotFound();
            }

            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Status item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            statusRepository.Add(item);

            return CreatedAtRoute("GetOneStatus", new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Status item)
        {
            if (item == null || item.Id != id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var status = statusRepository.GetOneById(id);
            if (status == null)
            {
                return NotFound();
            }

            // TODO: FAT CONTROLLER! Refactor
            if (item.Active.HasValue && item.Active.Value)
            {
                status.Activate();
            }
            else
            {
                status.Deactivate();
            }

            status.Code = item.Code;
            status.Name = item.Name;

            statusRepository.Update(status);

            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var status = statusRepository.GetOneById(id);
            if (status == null)
            {
                return NotFound();
            }

            statusRepository.Remove(status);
            return new NoContentResult();
        }
    }
}
