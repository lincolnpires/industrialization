namespace Industrialization.Domain.Abstract
{
    /// <summary>
    /// Represents an object that is distinguished by its IDENTITY, rather than its attributes.
    /// </summary>
    public abstract class Entity //<T>
    {
        /// <summary> Identifier of this object. </summary>
        public int Id { get; set; }
        // public T Id { get; set; }
        // public Guid Id { get; set; }

        // I'm keeping it simple without all the crazy details of the
        // entity identity implementation because this is only an evaluation.
    }
}
