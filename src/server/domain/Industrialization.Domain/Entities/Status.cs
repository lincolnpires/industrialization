using Industrialization.Domain.Abstract;
using System;
using System.Collections.Generic;

// pretty much the same as the other class (type)
namespace Industrialization.Domain.Entities
{
    public class Status : Entity
    {
        protected Status()
        {
            this.Users = new HashSet<User>();
        }

        public Status(string name, string code)
        {
            this.Name = name;
            this.Code = code;
            this.Active = true;

            this.Users = new HashSet<User>();
        }

        public void Activate() => this.Active = true;

        public void Deactivate() => this.Active = false;

        public string Name { get; set; }
        public string Code { get; set; }
        public bool? Active { get; set; }

        public ICollection<User> Users { get; set; }
    }
}
