using Industrialization.Domain.Abstract;
using System;

namespace Industrialization.Domain.Entities
{
    public class User : Entity
    {
        protected User() { /* I am not sure if EF Core need this, but EF5+ need for sure */ }

        public User (UserType type, Status status, string name, string email, string phone)
        {
            this.Name = name;
            this.Email = email;
            this.Phone = phone;

            this.UserType = type;
            this.Status = status;
        }

        public void SetNetworkLoginAndPassword(string login, string password)
        {
            this.NetworkLogin = login;
            this.Password = password;
        }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public string NetworkLogin { get; set; }
        public string Password { get; set; }

        public int TypeId { get; set; }
        public int StatusId { get; set; }

        public UserType UserType { get; set; }
        public Status Status { get; set; }
    }
}
