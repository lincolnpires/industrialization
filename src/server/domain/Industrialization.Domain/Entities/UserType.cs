using Industrialization.Domain.Abstract;
using System;
using System.Collections.Generic;

namespace Industrialization.Domain.Entities
{
    public class UserType : Entity
    {
        protected UserType()
        {
            this.Users = new HashSet<User>();
        }

        public UserType(string name, string code)
        {
            this.Name = name;
            this.Code = code;
            this.Active = true; // default

            this.Users = new HashSet<User>();
        }

        public void Activate() => this.Active = true;

        public void Deactivate() => this.Active = false;

        public string Name { get; set; }
        public string Code { get; set; }
        public bool? Active { get; set; }

        public ICollection<User> Users { get; set; }
    }
}
