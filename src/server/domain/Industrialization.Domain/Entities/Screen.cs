using Industrialization.Domain.Abstract;
using System;

namespace Industrialization.Domain.Entities
{
    public class Screen : Entity
    {
        public Screen() { }

        public Screen(
            string name, string description, string path, bool isAdmin, string thumbPath)
        {
            this.Name = name;
            this.Description = description;
            this.Path = path;
            this.IsAdmin = isAdmin;
            this.ThumbPath = thumbPath;
            this.Active = true;
        }

        public void Activate() => this.Active = true;

        public void Deactivate() => this.Active = false;

        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public bool IsAdmin { get; set; }
        public string ThumbPath { get; set; }
        public bool? Active { get; set; }
    }
}
