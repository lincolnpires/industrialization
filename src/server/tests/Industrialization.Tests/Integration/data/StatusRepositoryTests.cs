using System.Linq;
using System.Collections.Generic;
using Industrialization.Domain.Entities;
using Industrialization.Infrastructure.DataAccess.Abstract;
using Industrialization.Infrastructure.DataAccess.Concrete;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Industrialization.Tests.Integration.data
{
    public class StatusRepositoryTests
    {
        private AppDbContext dbContext;

        private IStatusRepository GetRepository()
        {
            var options = DbContextOptionsFactory.GetDbContextOptions();

            dbContext = new AppDbContext(options);
            var repository = new StatusRepository(dbContext);
            foreach (var item in GetSampleData())
            {
                repository.Add(item);
            }

            return repository;
        }

        private IEnumerable<Status> GetSampleData()
        {
            return new List<Status>(){
                new Status("Created", "0-c"),
                new Status("Aproved", "1-a"),
                new Status("Deleted", "2-d"),
                new Status("Suspended", "3-s")
            };
        }

        [Fact]
        public void Add()
        {
            var repository = GetRepository();
            var item = new Status("Status Name", "Status-Code");

            repository.Add(item);

            var retrieved = repository
                .GetAll()
                .FirstOrDefault(x => x.Name == item.Name);

            Assert.True(retrieved.Id != default(int));
            Assert.True(item.Code == retrieved.Code);
        }

        [Fact]
        public void Remove()
        {
            var repository = GetRepository();

            var toBeRemoved = repository.GetAll().LastOrDefault();

            var name = toBeRemoved.Name;
            var code = toBeRemoved.Code;

            repository.Remove(toBeRemoved);
            var result = repository.GetMany(x => x.Name == name && x.Code == code);
            Assert.Empty(result);
        }

        [Fact]
        public void Update()
        {
            var repository = GetRepository();
            var toBeUpdated = repository.GetAll().FirstOrDefault();
            var beforeUpdate = new Status(toBeUpdated.Name, toBeUpdated.Code);

            toBeUpdated.Code = System.Guid.NewGuid().ToString();
            bool isActive = false;
            if (toBeUpdated.Active.Value) {
                toBeUpdated.Deactivate();
                isActive = false;
            } else {
                toBeUpdated.Activate();
                isActive = true;
            }

            repository.Update(toBeUpdated);

            Assert.Empty(repository.GetMany(
                x => x.Name == beforeUpdate.Name
                && x.Code == beforeUpdate.Code
                && x.Active == isActive));

            Assert.Contains(toBeUpdated, repository.GetAll());
        }
    }
}
