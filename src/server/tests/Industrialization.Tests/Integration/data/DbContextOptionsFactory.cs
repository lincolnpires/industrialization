using Industrialization.Infrastructure.DataAccess.Concrete;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Industrialization.Tests.Integration.data
{
    public sealed class DbContextOptionsFactory
    {
        /// <summary>
        /// Creates context options for an in-memory instance of EF.
        /// </summary>
        /// <returns>
        /// The options to pass to the DbContext constructor
        /// </returns>
        public static DbContextOptions<AppDbContext> GetDbContextOptions()
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            var builder = new DbContextOptionsBuilder<AppDbContext>();

            builder.UseInMemoryDatabase("industrialization")
                   .UseInternalServiceProvider(serviceProvider);

            return builder.Options;
        }
    }
}
