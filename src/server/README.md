# Application / Services

Acts as the orchestrator which coordinates the application activity.
It does not hold the state of the business objects, but it can hold the state of an application task progress.


# Domain

Contains information about the domain. This is the heart of the business software.


# Infrastructure

Acts as a supporting library for all the other projects.
It also implements persistence for business objcts.


# Tests

Hold the tests for the server project.
