import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../models/user';
import { Type } from '../models/type';
import { Status } from '../models/status';

@Component({
  selector: 'app-user',
  templateUrl: './user-detail.component.html'
})
export class UserDetailComponent implements OnInit {
  pageTitle: string = 'User';
  error: string;
  user: User = {};
  id: number;
  status: Array<Status> = [];
  types: Array<Type> = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private appService: AppService) {
    this.error = '';
  }

  private initUser() {
    this.id = +this.route.snapshot.paramMap.get('id');

    if (this.id === 0) {
      return;
    }

    this.appService.getUser(this.id).subscribe(
      user => this.user = user,
      error => this.error += error
    );
  }

  private initStatus() {
    this.appService.getStatus().subscribe(status => {
      this.status = status.filter(x => x.active);

      if (this.status.length > 0) {
        if (this.id !== 0) {
          this.user.status = this.types.find(x => x.id === this.user.statusId);
        }
      } else {
        this.error += '<br> Please add at least one "active" Status';
      }
    }, error => this.error += error
    );
  }

  private initTypes() {
    this.appService.getTypes().subscribe(types => {
      this.types = types.filter(x => x.active);

      if (this.types.length > 0) {
        if (this.id !== 0) {
          this.user.userType = this.types.find(x => x.id === this.user.typeId);
        }
      } else {
        this.error += '<br> Please add at least one "active" Type';
      }
    }, error => this.error += error
    );
  }

  ngOnInit() {
    this.initUser();
    this.initStatus();
    this.initTypes();
  }

  onSubmit() {
    if (this.id === 0) {
      this.appService.postUser(this.user).subscribe(
        () => this.router.navigate(['/users']),
        error => this.error = error
      );
    } else {
      this.appService.putUser(this.user).subscribe(
        () => this.router.navigate(['/users']),
        error => this.error = error
      );
    }
  }

}
