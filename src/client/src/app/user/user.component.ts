import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { AppService } from '../app.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html'
})
export class UserComponent implements OnInit {
  pageTitle: string = 'Users';
  error: string;
  users: Array<User> = [];

  constructor(
    private appService: AppService
  ) { }

  ngOnInit(): void {
    this.initGrid();
  }

  private initGrid() {
    this.appService.getUsers().subscribe(
      users => this.users = users,
      error => this.error = error
    );
  }

  delete(id: number): void {
    this.appService.deleteUser(id).subscribe(
      () => this.initGrid(),
      error => this.error = error
    )
  }

}
