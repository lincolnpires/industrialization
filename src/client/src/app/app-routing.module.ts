import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ScreenComponent } from './screen/screen.component';
import { ScreenDetailComponent } from './screen/screen-detail.component';
import { StatusComponent } from './status/status.component';
import { TypeComponent } from './type/type.component';
import { UserComponent } from './user/user.component';
import { UserDetailComponent } from './user/user-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'screens', component: ScreenComponent },
  { path: 'status', component: StatusComponent },
  { path: 'types', component: TypeComponent },
  { path: 'users', component: UserComponent },
  { path: 'screens/:id', component: ScreenDetailComponent },
  { path: 'users/:id', component: UserDetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
