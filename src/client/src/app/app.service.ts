import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Observable } from "rxjs/Observable";
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from "rxjs/operators";
import 'rxjs/add/operator/catch';

import { environment } from '../environments/environment';

import { Screen } from './models/screen';
import { User } from './models/user';
import { Type } from './models/type';
import { Status } from './models/status';
import { $ } from 'protractor';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class AppService {

  // this is a shame, every model should have its individual service

  private readonly apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  getScreens(): Observable<Array<Screen>> {
    return this.http.get<Array<Screen>>(`${this.apiUrl}/screen`)
    .catch(this.handleError);
  }

  getScreen(id: number): Observable<Screen> {
    return this.http.get<Screen>(`${this.apiUrl}/screen/${id}`)
    .catch(this.handleError);
  }

  postScreen(body: Screen) : Observable<any> {
    return this.http.post(`${this.apiUrl}/screen`, body, httpOptions)
    .catch(this.handleError);
  }

  putScreen(body: Screen) : Observable<any> {
    return this.http.put(`${this.apiUrl}/screen/${body.id}`, body, httpOptions)
    .catch(this.handleError);
  }

  deleteScreen(id: number) : Observable<any> {
    return this.http.delete(`${this.apiUrl}/screen/${id}`, httpOptions)
    .catch(this.handleError);
  }

  getUsers(): Observable<Array<User>> {
    return this.http.get<Array<User>>(`${this.apiUrl}/user`)
    .catch(this.handleError);
  }

  getUser(id: number): Observable<User> {
    return this.http.get<User>(`${this.apiUrl}/user/${id}`)
    .catch(this.handleError);
  }

  postUser(body: User): Observable<any> {
    return this.http.post(`${this.apiUrl}/user`, body, httpOptions)
      .catch(this.handleError);
  }

  putUser(body: User): Observable<any> {
    return this.http.put(`${this.apiUrl}/user/${body.id}`, body, httpOptions)
      .catch(this.handleError);
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete(`${this.apiUrl}/user/${id}`, httpOptions)
      .catch(this.handleError);
  }

  getStatus(): Observable<Array<Status>> {
    return this.http.get<Array<Status>>(`${this.apiUrl}/status`)
    .catch(this.handleError);
  }

  getOneStatus(id: number): Observable<Status> {
    return this.http.get<Status>(`${this.apiUrl}/status/${id}`)
    .catch(this.handleError);
  }

  postStatus(body: Status): Observable<any> {
    return this.http.post(`${this.apiUrl}/status`, body, httpOptions)
      .catch(this.handleError);
  }

  putStatus(body: Status): Observable<any> {
    return this.http.put(`${this.apiUrl}/status/${body.id}`, body, httpOptions)
      .catch(this.handleError);
  }

  deleteStatus(id: number): Observable<any> {
    return this.http.delete(`${this.apiUrl}/status/${id}`, httpOptions)
      .catch(this.handleError);
  }

  getTypes(): Observable<Array<Type>> {
    return this.http.get<Array<Type>>(`${this.apiUrl}/type`)
    .catch(this.handleError);
  }

  getType(id: number): Observable<Type> {
    return this.http.get<Type>(`${this.apiUrl}/type/${id}`)
    .catch(this.handleError);
  }

  postType(body: Type): Observable<any> {
    return this.http.post(`${this.apiUrl}/type`, body, httpOptions)
      .catch(this.handleError);
  }

  putType(body: Type): Observable<any> {
    return this.http.put(`${this.apiUrl}/type/${body.id}`, body, httpOptions)
      .catch(this.handleError);
  }

  deleteType(id: number): Observable<any> {
    return this.http.delete(`${this.apiUrl}/type/${id}`, httpOptions)
      .catch(this.handleError);
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   */
  private handleError(error) {
      // TODO: send the error to remote logging infrastructure
      console.error("AppService - Something went wrong: ", error); // log to console instead

      // Let the app keep running by returning of.
      return of(error);
  }

}
