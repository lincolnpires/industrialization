import { Component, OnInit } from '@angular/core';
import { Screen } from '../models/screen';
import { AppService } from '../app.service';

@Component({
  selector: 'app-screen',
  templateUrl: './screen.component.html'
})
export class ScreenComponent implements OnInit {
  pageTitle: string = 'Screens';
  error: string;
  screens: Array<Screen> = [];

  constructor(
    private appService: AppService
  ) { }

  ngOnInit(): void {
    this.initGrid();
  }

  private initGrid() {
    this.appService.getScreens().subscribe(
      screens => this.screens = screens,
      error => this.error = error
    );
  }

  delete(id: number): void {
    this.appService.deleteScreen(id).subscribe(
      () => this.initGrid(),
      error => this.error = error
    )
  }

}
