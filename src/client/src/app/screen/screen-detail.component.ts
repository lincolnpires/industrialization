import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Screen } from '../models/screen';

@Component({
  selector: 'app-screen',
  templateUrl: './screen-detail.component.html'
})
export class ScreenDetailComponent implements OnInit {
  pageTitle: string = 'Screen';
  error: string;
  screen: Screen = {};
  id: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private appService: AppService) { }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');

    if (this.id === 0) {
      return;
    }

    this.appService.getScreen(this.id).subscribe(
      screen => this.screen = screen,
      error => this.error = error
    );
  }

  onSubmit() {
    if (this.id === 0) {
      this.appService.postScreen(this.screen).subscribe(
        () => this.router.navigate(['/screens']),
        error => this.error = error);
    } else {
      this.appService.putScreen(this.screen).subscribe(
        () => this.router.navigate(['/screens']),
        error => this.error = error
      );
    }
  }

}
