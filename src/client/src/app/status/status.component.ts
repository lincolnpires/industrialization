import { Component, OnInit } from '@angular/core';
import { Status } from '../models/status';
import { AppService } from '../app.service';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html'
})
export class StatusComponent implements OnInit {
  pageTitle: string = 'Status';
  error: string;
  status: Array<Status> = [];

  constructor(
    private appService: AppService
  ) { }

  ngOnInit(): void {
    this.appService.getStatus().subscribe(
      status => this.status = status,
      error => this.error = error
    );
  }

}
