export class Screen {
  id?: number;
  name?: string;
  description?: string;
  path?: string;
  isAdmin?: boolean;
  thumbPath?: string;
  active?: boolean;
}
