import { User } from "./user";

export class Type {
  id?: number;
  name?: string;
  code?: string;
  active?: boolean;
}
