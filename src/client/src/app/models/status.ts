import { User } from "./user";

export class Status {
  id?: number;
  name?: string;
  code?: string;
  active?: boolean;
}
