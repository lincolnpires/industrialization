import { Type } from "./type";
import { Status } from "./status";

export class User {
  id?: number;
  name?: string;
  email?: string;
  phone?: string;
  networkLogin?: string;
  password?: string;
  typeId?: number;
  statusId?: number;
  userType?: Type;
  status?: Status;
}
