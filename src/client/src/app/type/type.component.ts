import { Component, OnInit } from '@angular/core';
import { Type } from '../models/type';
import { AppService } from '../app.service';

@Component({
  selector: 'app-type',
  templateUrl: './type.component.html'
})
export class TypeComponent implements OnInit {
  pageTitle: string = 'Types';
  error: string;
  types: Array<Type> = [];

  constructor(
    private appService: AppService
  ) { }

  ngOnInit(): void {
    this.appService.getTypes().subscribe(
      types => this.types = types,
      error => this.error = error
    );
  }

}
