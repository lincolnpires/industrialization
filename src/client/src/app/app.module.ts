import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { ScreenComponent } from './screen/screen.component';
import { ScreenDetailComponent } from './screen/screen-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { StatusComponent } from './status/status.component';
import { TypeComponent } from './type/type.component';
import { UserComponent } from './user/user.component';
import { UserDetailComponent } from './user/user-detail.component';

import { AppRoutingModule } from "./app-routing.module";


@NgModule({
  declarations: [
    AppComponent,
    ScreenComponent,
    ScreenDetailComponent,
    DashboardComponent,
    StatusComponent,
    TypeComponent,
    UserComponent,
    UserDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
